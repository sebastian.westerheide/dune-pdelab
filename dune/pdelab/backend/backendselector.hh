#ifndef DUNE_PDELAB_BACKENDSELECTOR_HH
#define DUNE_PDELAB_BACKENDSELECTOR_HH

#warning This file has been deprecated and will be removed after the release of PDELab 2.4. Use dune/pdelab/backend/interface.hh instead

#include <dune/pdelab/backend/interface.hh>

#endif

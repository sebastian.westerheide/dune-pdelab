// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:
#ifndef DUNE_ISTLSOLVERBACKEND_HH
#warning "The file dune/pdelab/backend/istlsolverbackend.hh is deprecated. Please use dune/pdelab/backend/istl.hh instead."
#include <dune/pdelab/backend/istl.hh>
#endif
